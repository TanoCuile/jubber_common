"use strict";
var i18n_1 = require("./components/i18n");
var JubberCommon = (function () {
    function JubberCommon() {
    }
    JubberCommon.i18n = function (lang) {
        return i18n_1.default(lang);
    };
    return JubberCommon;
}());
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = JubberCommon;
//# sourceMappingURL=index.js.map