export default function (lang) {
	return require('../assets/i18n/' + lang + '.json');
}