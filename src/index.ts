import i18n from './components/i18n';

export default class JubberCommon{
	static i18n(lang) {
		return i18n(lang);
	}
}